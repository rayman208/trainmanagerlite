﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace TrainManagerLite
{
    public partial class FormMain : Form
    {
        MySqlConnection connection;
        MySqlCommand command;

        private void RefreshDataGridViewArrivals()
        {
            command.CommandText = @"SELECT * FROM arrivals";

            dataGridViewArrivals.Rows.Clear();
            MySqlDataReader reader = command.ExecuteReader();

            while (reader.Read() == true)
            {
                int id = reader.GetInt32("id");
                string code = reader.GetString("code");
                string source = reader.GetString("source");
                DateTime datetime = reader.GetDateTime("datetime");
                string type = reader.GetString("type");

                dataGridViewArrivals.Rows.Add(id, code, source, datetime, type);
            }

            reader.Close();
        }

        private void FillFields()
        {
            if (dataGridViewArrivals.SelectedRows.Count == 0)
            { return; }

            textBoxId.Text = dataGridViewArrivals.SelectedRows[0].Cells[0].Value.ToString();

            textBoxCode.Text = dataGridViewArrivals.SelectedRows[0].Cells[1].Value.ToString();

            textBoxSource.Text = dataGridViewArrivals.SelectedRows[0].Cells[2].Value.ToString();

            dateTimePickerDateTime.Value = DateTime.Parse(dataGridViewArrivals.SelectedRows[0].Cells[3].Value.ToString());

            for (int i = 0; i < comboBoxType.Items.Count; i++)
            {
                if (comboBoxType.Items[i].ToString() == dataGridViewArrivals.SelectedRows[0].Cells[4].Value.ToString())
                {
                    comboBoxType.SelectedIndex = i;
                    break;
                }
            }
        }

        private void ClearFields()
        {
            textBoxId.Clear();
            textBoxCode.Clear();
            textBoxSource.Clear();
            dateTimePickerDateTime.Value = DateTime.Now;
            comboBoxType.SelectedIndex = 0;
        }

        private bool IsFillFields()
        {
            return textBoxCode.Text != String.Empty && textBoxSource.Text != String.Empty;
        }

        public FormMain()
        {
            InitializeComponent();
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            if (!IsFillFields())
            {
                MessageBox.Show("Ошибка. Заполните все поля!");
                return;
            }

            command.CommandText = $@"INSERT INTO arrivals (code,source,datetime,type)
              VALUES(
                '{textBoxCode.Text}',
                '{textBoxSource.Text}',
                '{dateTimePickerDateTime.Value.ToString("yyyy-MM-dd H:mm:ss")}',
                '{comboBoxType.SelectedItem.ToString()}')";

            command.ExecuteNonQuery();

            RefreshDataGridViewArrivals();

            ClearFields();
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (textBoxId.Text == string.Empty)
            {
                MessageBox.Show("Ошибка. Выберите строку для удаления!");
                return;
            }

            command.CommandText = $"DELETE FROM arrivals WHERE id={textBoxId.Text}";
            command.ExecuteNonQuery();

            RefreshDataGridViewArrivals();

            ClearFields();
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            if (textBoxId.Text == string.Empty)
            {
                MessageBox.Show("Ошибка. Выберите строку для обновления!");
                return;
            }

            if (!IsFillFields())
            {
                MessageBox.Show("Ошибка. Заполните все поля!");
                return;
            }

            command.CommandText = $@"UPDATE arrivals SET
            code='{textBoxCode.Text}',
            source='{textBoxSource.Text}',
            datetime='{dateTimePickerDateTime.Value.ToString("yyyy-MM-dd H:mm:ss")}',
            type='{comboBoxType.SelectedItem.ToString()}'
            WHERE id={textBoxId.Text}";
            
            command.ExecuteNonQuery();

            RefreshDataGridViewArrivals();

            ClearFields();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            string connectionString = "Server=localhost;Port=3306;Database=train_manager_lite;User=root;Password=1234";
            connection = new MySqlConnection(connectionString);
            connection.Open();

            command = new MySqlCommand();
            command.Connection = connection;

            comboBoxType.SelectedIndex = 0;

            RefreshDataGridViewArrivals();
        }

        private void dataGridViewArrivals_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            FillFields();
        }
    }
}
